var ctx = document.getElementById('bar').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ['JAN', 'FEB', 'MARCH', 'APRIL', 'MAY', 'JUNE'],
        datasets: [{
            label: 'Borrowed Books, Returned Books',
            data: [3, 9, 5, 12, 6, 2],
            backgroundColor: [
                'rgba(192,57,43 ,1)',
                'rgba(41,128,185 ,1)',
                'rgba(192,57,43 ,1)',
                'rgba(41,128,185 ,1)',
                'rgba(192,57,43 ,1)',
                'rgba(41,128,185 ,1)'
            ],
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});


var pie = document.getElementById('pie').getContext('2d');
var myChart = new Chart(pie, {
    type: 'pie',
    data: {
        labels: ['Novel', 'Fiction', 'Biography', 'Horror'],
        datasets: [{
            label: '# of Votes',
            data: [12, 5, 9, 10],
            backgroundColor: [
                'rgba(64, 64, 122,1.0)',
                'rgba(204, 142, 53,1.0)',
                'rgba(255, 82, 82,1.0)',
                'rgba(34, 112, 147,1.0)'
                
            ],
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});



