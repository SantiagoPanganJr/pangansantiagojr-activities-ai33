export default {
    data() {
   return {
     Form: {
       bname: "",
       bAuthor: "",
       bcopies: "",
       bcategory: "",
       BookDateRegistered: new Date(),
     },
     BookModel: [],
     SelectedBook: "",
     Search: "",
   };
 },
 methods: {
   AddBook() {
     this.BookModel.unshift(this.Form);
     console.log(this.form);
     this.HideModal();
   },
   UpdateBook(index) {
     console.log(index);
     this.BookModel.splice(index, 1, this.Form);

     this.HideModal();
   },
   ToBeUpdated(index) {
     this.SelectedBook = index;
     this.Form.bname = this.BookModel[index].bname;
     this.Form.bAuthor = this.BookModel[index].bAuthor;
     this.Form.bcopies = this.BookModel[index].bcopies;
     this.Form.bcategory = this.BookModel[index].bcategory;
   },
   DeleteBook(index) {
     this.BookModel.splice(index, 1);

     this.HideModal();
   },
   HideModal() {
     this.$refs.modal.hide();
     this.FormReset();
   },
   FormReset() {
     this.Form = {
       bname: "",
       bAuthor: "",
       bcopies: "",
       bcategory: "",
       BookDateRegistered: new Date(),
     };

     this.SelectedBook = "";
   },
 },
 computed: {
   BookModelFilter() {
     return this.BookModel.filter((Book) => {
       return this.Search.toLowerCase()
         .split(" ")
         .every((v) => Book.bname.toLowerCase().includes(v));
     });
   },
 },
};
