<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BookRequest extends FormRequest
{
    
    public function authorize()
    {
        return true;
    }

    
    public function rules()
    {
        return [
            'name' => 'required|max:30',
            'author' => 'required|max:30',
            'copies' => 'required|integer|max:100',
            'category_id' => 'required|integer'
            
        ];
    }
}
