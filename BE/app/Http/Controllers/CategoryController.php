<?php

namespace App\Http\Controllers;

use App\Models\Category;

use App\Http\Requests\CategoryRequest;
use Illuminate\Http\Request;

class CategoryController extends Controller
{

    public function index()
    {
        $categories = Category::all();
        return response()->json([
            "message" => "Category List",
            "data" => $categories]);
    }

    
    public function store(CategoryRequest $request)
    {
        $categories = new Category();

        $categories->category = $request->category;
       
        $validated = $request->validated();
        $categories->save();
        return response()->json($categories);
    }

   
    public function show($id)
    {
        $categories = Category::find($id);
        return response()->json($categories);
    }

   
    public function update(CategoryRequest $request, $id)
    {
        $categories = Category::find($id);
        $categories->category = $request->category;

        $validated = $request->validated();
        $categories->update();
        return response()->json($categories);
    }
    

    public function delete(CategoryRequest $request, $id)
    {
        $categories = Category::find($id);
        $categories->delete();
        return response()->json($categories);

    }

   
}
