<?php

namespace App\Http\Controllers;

use App\Models\ReturnedBook;
use Illuminate\Http\Request;

class ReturnedBookController extends Controller
{
    public function index()
    {
        $returnedbook = ReturnedBook::all();
        return response()->json([
            "message" => "Returned Books",
            "data" => $returnedbook]);
    }

   
    public function create()
    {
        
    }

   
    public function store(Request $request)
    {
        
    }

  
    public function show($id)
    {
       
    }

   
    public function edit($id)
    {
        
    }

    
    public function update(Request $request, $id)
    {
       
    }

    
    public function destroy($id)
    {
        
    }
}
